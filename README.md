# scala-js-broken-telephone
Implementation of broken telephone game in scala-js using google translate API.

## Why?
This project has been created in order to evaluate
the quality of google translator in a series of translation
steps.

## How it works
This project works only in local browser using scala js and google translator API (see https://ctrlq.org/code/19909-google-translate-api).

## Build
* `sbt ~fastOptJS` - for development
* `sbt fullOptJS` - for production
* `target/scala-2.11/classes/` - folder for output HTML files

## License
GNU Version 3
