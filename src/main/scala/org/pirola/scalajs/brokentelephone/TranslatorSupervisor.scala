package org.pirola.scalajs.brokentelephone

import akka.actor._
import java.time.Instant
import java.time.temporal.ChronoUnit
import org.pirola.scalajs.brokentelephone.Languages.Language
import scala.collection.mutable.MutableList
import akka.actor.SupervisorStrategy._

package translator.supervisor {

  package event {
    case class EndTranslation(translationSteps: MutableList[TranslationStep])

    case class TranslationFailed(exception: Exception)
  }
  package command {
    case class Translate(language: Language, message: String, numberOfSteps: Int)
  }
}

case class TranslationStep(languageFrom: Language, languageTo: Language, message: String)

object TranslatorSupervisor {
  def props(): Props = {
    Props(new TranslatorSupervisor())
  }
}

class TranslatorSupervisor extends Actor with ActorLogging {

  private var countMsgTranslated = 0
  private var numberOfSteps = 0
  private var originalSender: ActorRef = _
  private var startLanguage: Language = _
  private val translationSteps: MutableList[TranslationStep] = MutableList.empty

  val instantStart = Instant.now()

  override def preStart(): Unit = {
    log.info(s"Start translator supervisor")
  }

  override val supervisorStrategy =
    OneForOneStrategy() {
      case exc: Exception =>
        log.info(s"An error occurred in message translation [${exc}]")
        originalSender ! translator.supervisor.event.TranslationFailed(exc)
        Escalate
    }

  override def postStop(): Unit = {
    val instantStop = Instant.now()
    val msElapsed = ChronoUnit.MILLIS.between(instantStart, instantStop)
    log.info(s"Stop translator supervisor UP&Running for $msElapsed ms")
  }

  def receive = {
    case x: translator.supervisor.command.Translate =>
      numberOfSteps = x.numberOfSteps
      startLanguage = x.language
      log.debug(s"received sending answer x[${x}]")
      createChild(x.language, x.message)
      countMsgTranslated = 0
      originalSender = sender()
    case x: translator.worker.event.MessageTranslated if countMsgTranslated == numberOfSteps =>
      log.debug(
        s"Received message translated x[${x}] countMessageTranslated[${countMsgTranslated}]")
      addToTranslated(x)
      createChild(x.languageTo, startLanguage, x.message)
      countMsgTranslated += 1
    case x: translator.worker.event.MessageTranslated if countMsgTranslated >= numberOfSteps =>
      log.debug(
        s"Received message translated x[${x}] countMessageTranslated[${countMsgTranslated}]")
      addToTranslated(x)
      log.info(s"All translations [${translationSteps}]")
      originalSender ! translator.supervisor.event.EndTranslation(translationSteps)
    case x: translator.worker.event.MessageTranslated =>
      log.debug(
        s"Received message translated x[${x}] countMessageTranslated[${countMsgTranslated}]")
      addToTranslated(x)
      createChild(x.languageTo, x.message)
      countMsgTranslated += 1
  }

  def addToTranslated(msg: translator.worker.event.MessageTranslated): Unit =
    translationSteps += TranslationStep(msg.languageFrom, msg.languageTo, msg.message)

  private def createChild(languageFrom: Language, message: String): (ActorRef, Language) = {
    val randomLanguage = Languages.randomLanguage
    val child = context.actorOf(TranslatorWorker.props(languageFrom, randomLanguage, message))
    (child, randomLanguage)
  }
  private def createChild(languageFrom: Language,
                          languageTo: Language,
                          message: String): (ActorRef, Language) = {
    val child = context.actorOf(TranslatorWorker.props(languageFrom, languageTo, message))
    (child, languageTo)
  }
}
