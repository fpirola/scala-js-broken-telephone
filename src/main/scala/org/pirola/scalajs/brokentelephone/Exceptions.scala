package org.pirola.scalajs.brokentelephone

case class WorkerException(message: String, cause: Throwable = null)
    extends Exception(message, cause)

case class NoTranslationFoundException(message: String) extends Exception(message)
