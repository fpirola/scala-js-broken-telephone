package org.pirola.scalajs.brokentelephone

import java.time.Instant

import scala.language.implicitConversions
import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}
import org.scalajs.dom._
import org.scalajs.dom

import scala.collection.mutable.{ArrayBuffer => MutableArray}
import dom.html
import org.scalajs.dom.ext.Ajax
import java.util.UUID.randomUUID
import akka.actor._
import scala.concurrent.duration._
import akka.pattern.ask
import akka.actor.SupervisorStrategy._

object BrokenTelephone {

  @JSExportTopLevel("translate")
  def translate(textToTranslate: html.TextArea, output: dom.Node) = {
    val systemName = s"broken-telephone-${randomUUID}"
    val system = ActorSystem(systemName)

    while (output.firstChild != null) {
      output.removeChild(output.firstChild)
    }

    def ppActor() =
      Props(new Actor with ActorLogging {
        val translatorSupervisor = system.actorOf(TranslatorSupervisor.props())

        implicit val timeout: akka.util.Timeout = 5.seconds
        import akka.pattern.pipe
        import context.dispatcher
        val result = translatorSupervisor ? translator.supervisor.command
          .Translate(Languages.getByIsoCode("en").get, textToTranslate.value, 2)
        result pipeTo self
        def receive = {
          case endTrans: translator.supervisor.event.EndTranslation =>
            log.info(s"received $endTrans sending answer")
            endTrans.translationSteps.map { x =>
              appendChild(
                s"${x.message} -- From: ${x.languageFrom.name} --> To: ${x.languageTo.name}")
            }
          case translator.supervisor.event.TranslationFailed(exc) =>
            log.info(s"An error occurred in message translation")
            appendChild(s"An error occurred in message translation, please retry")
        }

        def appendChild(text: String) = {
          val parNode = document.createElement("p")
          val textNode = document.createTextNode(text)
          parNode.appendChild(textNode)
          output.appendChild(parNode)

        }
      })
    val pinger = system.actorOf(ppActor())
  }
}
