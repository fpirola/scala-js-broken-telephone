package org.pirola.scalajs.brokentelephone

import akka.actor._
import org.scalajs.dom.ext.Ajax
import org.scalajs.dom.raw.XMLHttpRequest
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import akka.pattern.pipe

import java.util.regex.Pattern
import org.pirola.scalajs.brokentelephone.Languages._

package translator.worker {
  package command {
    case class MessageToTranslate(message: String)
  }
  package event {
    case class MessageTranslated(languageFrom: Language, languageTo: Language, message: String)
  }
}

object TranslatorWorker {
  def props(languageFrom: Language, languageTo: Language, message: String): Props = {
    Props(new TranslatorWorker(languageFrom, languageTo, message))
  }
  val HttpStatusOk = 200
  private val p = Pattern.compile("""^\[\[\["([^"]+)""")
  def extractFirstTranslation(input: String): Option[String] = {
    val m = p.matcher(input)
    if (m.find && m.groupCount >= 1) Some(m.group(1))
    else None
  }
}

class TranslatorWorker(languageFrom: Language, languageTo: Language, message: String)
    extends Actor
    with ActorLogging {
  log.debug(s"Starting translator worker")

  val url =
    s"https://translate.googleapis.com/translate_a/single?client=gtx&sl=${languageFrom.isoCode}&tl=${languageTo.isoCode}&dt=t&q=${message}"
  Ajax.get(url) pipeTo self

  def receive = {
    case x: XMLHttpRequest if x.status == TranslatorWorker.HttpStatusOk =>
      log.debug(s"Received XMLHttpRequest x[${x.responseText}]")
      val messageTranslated = TranslatorWorker.extractFirstTranslation(x.responseText)
      messageTranslated match {
        case Some(y) =>
          context.parent ! translator.worker.event.MessageTranslated(languageFrom, languageTo, y)
        case None =>
          throw NoTranslationFoundException(s"No translation found in response[${x.responseText}]")
      }
      context.stop(self)
    case x: XMLHttpRequest =>
      val msg = s"Received XMLHttpRequest with errors x[${x}]"
      log.warning(msg)
      throw WorkerException(msg)
  }

}
