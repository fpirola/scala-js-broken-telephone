package org.pirola.scalajs.brokentelephone.test

import org.pirola.scalajs.brokentelephone.TranslatorWorker
import utest._

object TranslatorWorkerSpec extends TestSuite {
  def tests = this {
    "extractFirstTranslation - Empty string return None" - {
      val result = TranslatorWorker.extractFirstTranslation("")
      assert(result.isEmpty)
    }
    "extractFirstTranslation - Unparsable string return None" - {
      val result = TranslatorWorker.extractFirstTranslation("abcdefg[sdds")
      assert(result.isEmpty)
    }
    "extractFirstTranslation - Correct string " - {
      val result = TranslatorWorker.extractFirstTranslation(
        """[[["Ciao mondo!!","Hello World!!",,,1]],,"en"]""")
      assert(result.isDefined)
      assert(result == Some("Ciao mondo!!"))
    }
  }
}
