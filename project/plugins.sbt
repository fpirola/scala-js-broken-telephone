logLevel := Level.Warn

addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.17")

libraryDependencies += "com.geirsson" %% "scalafmt-bootstrap" % "0.6.6"
