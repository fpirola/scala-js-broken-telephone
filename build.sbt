import sbt.Keys._

val commonSettings = Seq(
  scalaVersion := "2.11.11",
  name := """scala-js-broken-telephone""",
  version := "1.0"
)

lazy val root = (project in file("."))
  .settings(commonSettings: _*)
  .enablePlugins(ScalaJSPlugin)
  .settings(
    libraryDependencies ++= Dependencies.library ++ Seq(
      "org.scala-js" %%% "scalajs-dom" % "0.9.2",
      "com.lihaoyi" %%% "scalarx" % "0.3.2",
      "org.scala-js" %%% "scalajs-java-time" % "0.2.1",
      "org.akka-js" %%% "akkajsactor" % "1.2.5.2",
      "com.lihaoyi" %%% "utest" % "0.4.7" % "test"
    ),
    testFrameworks += new TestFramework("utest.runner.Framework"),
    resolvers ++= Dependencies.resolvers,
    scalacOptions ++= Seq("-feature", "-language:_", "-unchecked", "-deprecation")
  )

def latestScalafmt = "0.6.6"
commands += Command.args("scalafmt", "Run scalafmt cli.") {
  case (state, args) =>
    val Right(scalafmt) =
      org.scalafmt.bootstrap.ScalafmtBootstrap.fromVersion(latestScalafmt)
    scalafmt.main("--non-interactive" +: args.toArray)
    state
}

onLoad in Global := (Command.process("scalafmt", _: State)) compose (onLoad in Global).value
